<?php

/**
 * Allow/deny libraries per module and per user role (authenticated/anonymous)
 */
function disable_libraries_library_info_alter(&$libraries, $extension) {
  \Drupal\disable_libraries\LibraryDisabler::handleLibraries($libraries, $extension);
}

/**
 * Alter hooks
 */
function disable_libraries_module_implements_alter(&$implementations, $hook) {

  /**
   * Make sure to execute last after all other modules implemented their changes
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Extension!module.api.php/function/hook_module_implements_alter/10
   */
  if ($hook === 'library_info_alter') {
    $group = $implementations['disable_libraries'];
    unset($implementations['disable_libraries']);
    $implementations['disable_libraries'] = $group;
  }

  /**
   * Disable requirements hook of photoswipe module if it's CDN based library
   * is disabled.
   *
   * Keeping it caused a weird chain of
   * - accessing array key of type boolean
   * - passing null instead of string to `Url::fromUri()`
   *   (also can't handle empty string)
   * - passing null instead of Url object to `Link::fromTextAndUrl()`
   *   (also can't handle string)
   * - causing fatal error when opening the config page `/admin/config`
   *   with useless error message
   * - populating useful error messages on subsequent/other pages
   *
   * @see web/modules/contrib/photoswipe/photoswipe.install
   */
  if (($hook === 'requirements') && isset($implementations['photoswipe'])) {

    /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery */
    $libraryDiscovery = \Drupal::service('library.discovery');
    $libraryDefinition = $libraryDiscovery->getLibraryByName('photoswipe', 'photoswipe.cdn');

    if (!$libraryDefinition || !isset($libraryDefinition['remote'])) {
      unset($implementations['photoswipe']);
    }
  }
}
