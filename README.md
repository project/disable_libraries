# Disable libraries

Use allow and block lists for libraries in [Drupal][1], differentiated by anonymous and authenticated users.

## Table of contents

- Features
- Usage
- Side effects
- Motivation and technical background
- Similar projects
- Development
- Copyright and License

## Features

- [x] disable CSS and JS libraries differentiated by anonymous and authenticated users
- [x] allow/deny complete modules or only some libraries of modules
- [x] disable external libraries of all modules (experimental)
- [x] disable CDN usage of webform module

### TODO

- [ ] add new state to distinguish between active theme, e. g. `{admin-theme}:authenticated` and `{theme}:authenticated`
  - use case:
    - minimal allow list for `{theme}:anonymous`
    - allow list for `{theme}:authenticated` with a few modules like toolbar
    - deny list for `{admin-theme}:authenticated` because there are too many libraries to use an allow list
- [ ] disable only CSS of themes and modules (for CSS Naked Day)
- [ ] disable only JS of themes and modules (for JS Naked Day)

## Usage

By default a allow list is used for anonymous visitors and a deny list for authenticated users. The active theme and it's parent/base themes are also allowed by default.

To get the old behavior, set the list mode for anonymous visitors from "allow" to "deny".

Allow and deny lists aren't active at the same time. Depending on the list mode, one or the other is used. But both lists are stored independently so you can switch between modes with preconfigured lists.

### Allow lists example

```
system
views/views.module
views/views.ajax/js
```

### Deny lists example

```
webform
contact
system/base
views/views.ajax/js
```

## Side effects

This module adds a cache context to `library_info:theme_name:<anonymous|authenticated>` in the `cache_discovery` table. Without this module, two cached keys are expected:

- `library_info:claro` (admin theme)
- `library_info:theme_name` (theme)

In theory, the admin theme can't be loaded as anonymous user, so three cached keys are expected:

- `library_info:claro:authenticated` (admin theme)
- `library_info:theme_name:anonymous` (theme)
- `library_info:theme_name:authenticated` (theme)

## Motivation and technical background

While writing a minimal base theme for Drupal ([drinimal][3]), I got stuck fighting the amount of randomly inserted CSS and JS libraries from core and contrib modules. Instead of investigating further, which form field or which setting may add more JS bloat or may break my layout, I decided to go the radical route: Disable __all__ libraries for anonymous users.

First I tried the `HOOK_page_attachments_alter` hook via theme, but it didn't catch dynamically added libraries. Than I tried to use `HOOK_library_info_alter` via module with a check, if the current user is authenticated. This looked promising, but the hook is globally - for anonymous and authenticated users. I got stuck and found a [partial solution by Orlando Thöny on drupal.stackexchange.com][6]. Now I had a modified cache context `library_info:theme_name:<anonymous|authenticated>` and `HOOK_library_info_alter` became usable.

Instead of using this technique as a quick and dirty hack in a custom module or in a theme, I wanted to write a general purpose module. All looked good, but soon I ran into 500 errors caused by `libraries_extend` keys of the claro theme. Again, the quick and dirty workaround would be to prevent disabling libraries of the active theme, if it extends other libraries. A cleaner way was to decorate the `theme.initialization` service and to remove all denied libraries from the active theme.

Because this whole problem became surprisingly complex, I finally started to learn phpunit, which was on my task list for a few years now. I began liking test driven development - especially when too much (mental) recursion and tons of edge cases are involved.

### Notes

#### Blocking (parts of) active theme could cause 500 error (fixed)

There is no alter hook for overwriting `libraries_extend` keys in the active theme. Extending libraries happens after disabling libraries. If the active theme extends other libraries with now non-existing libraries, exceptions are thrown.

##### Steps to reproduce

- authenticated in allow mode
- allowThemeAutomatically: false
- allow list: system
- error:

```
InvalidLibrariesExtendSpecificationException
HTTP 500 Internal Server Error
The specified library "claro/system.admin" does not exist.
```

or in reverse:

- authenticated in deny mode
- allowThemeAutomatically: false
- deny list: claro
- error:

```
InvalidLibrariesExtendSpecificationException
HTTP 500 Internal Server Error
The specified library "claro/ajax" does not exist.
```

#### Other options

If I wouldn't use two contexts (anonymous, authenticated) and two modes (allow, deny), I could write a decorator for `Drupal\Core\Extension\ExtensionList::createExtensionInfo()`. Instead of the decorator `HOOK_system_info_alter($info, $extension, $type)` (invoked in `Drupal\Core\Extension\ExtensionList::doList()`) should work, too. But I assume, that this list is cached so early, that the user object is not around yet and therefore I can't try to set a cache context there. And cache keys are hard coded in many places, so I'm not sure if adding a custom cache context somewhere in this area has side effects. Also I can't decorate `ExtensionList` directly, because it is no service. I would have to decorate `ThemeExtensionList` and `ModuleExtensionList` instead. They run `parent::doList()`, so I can't just overwrite `ExtensionList::getListCacheId()` with a custom cache context. Another (big) reason against decorating these things is, that [they are marked as not stable][5].

It looks like disabling libraries with just one mode would be best done during `HOOK_system_info_alter`.

If a future Drupal release brings a new hook in `Drupal\Core\Extension\Extension` or if it becomes a service, it might be worth rewriting this module again.

## Similar projects

- [Library manager][4] (not tested, yet) - can be used to overwrite libraries, but it doesn't differentiate between authenticated and anonymous users. From skimming the source code it also doesn't look like disabling libraries is possible.

## Development

Publishing directly on drupal.org is an experiment. On the one hand I want to avoid using too much of the infrastructure for the following reasons:

- no markdown support (project description, release info, issues...)
- only partial HTML support (this is fine)
- inconsistent HTML support between issues and forum (slightly annoying)
- invalid HTML support (this is bad) - see: [How to convert markdown into Drupal forum/issue compatible format?][7]
- no release automation (slightly annoying) - see:
  - [Automate publishing releases (like packagist with webhooks)][8]
  - [my notes about release workflow][9]
- privacy concerns on drupal.org:
  - has it's own user tracking (OKish, `init.js` is blocked by uBlock Origin)
  - It loads `fonts.googleapis.com` and `www.googletagmanager.com` (annoying) - without consent (disrespectful, violates GDPR)
  - but it's also usable with uBlock Origin (nice) and even usable with completely disabled JS (great)

On the other hand, I want to learn more about the culture on drupal.org. And of course the project visibility is much higher than releasing via codeberg.org (git) and packagist.org (releases).

Hopefully everything works without too many headaches. If the experiment fails, I can still move the project to an external host.

### Releases

#### Initial release

- [Meaning of module categories][10]
- again: no markdown support :-(

#### New releases

TODO

### Tests

#### Prepare test setup

```bash
mkdir test && cd test
composer create-project drupal/recommended-project:11.0.0-beta1@beta .
composer require --dev drupal/core-dev
composer require drush/drush

mkdir -p web/modules/contrib
git clone https://git.drupalcode.org/project/disable_libraries.git web/modules/contrib/disable_libraries

mkdir -p db
./vendor/bin/drush site:install --yes --db-url="sqlite://../db/test.sqlite"

# enable module
./vendor/bin/drush pm:install disable_libraries

cp ./web/core/phpunit.xml.dist ./web/core/phpunit.xml
# setup db for kernel test
sed -i -E 's~<env name="SIMPLETEST_DB" value=""/>~<env name="SIMPLETEST_DB" value="sqlite://../../db/kernel-test.sqlite"/>~g' ./web/core/phpunit.xml

# only needed for testing with next major update
# sed -i -E 's~"drupal/core": "^10"~"drupal/core": "^10 | ^11"~g' web/modules/contrib/disable_libraries/composer.json
# sed -i -E 's~core_version_requirement: ^10~core_version_requirement: ^10 | ^11~g' web/modules/contrib/disable_libraries/composer.json
```

#### Run tests

```bash

# running phpunit from root doesn't work
# see: https://www.drupal.org/project/drupal/issues/3445847
cd web
../vendor/bin/phpunit -c core/phpunit.xml modules/contrib/disable_libraries/tests/src/Kernel/LibraryDisablerTest.php
../vendor/bin/phpunit -c core/phpunit.xml modules/contrib/disable_libraries/tests/src/Unit/LibraryDisablerTest.php
```

## Copyright and License

Copyright 2024 Raffael Jesche under the [GPL-2.0-or-later][2] license.

See `LICENSE.txt` for more information.

[1]: https://www.drupal.org/
[2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[3]: https://www.drupal.org/project/drinimal
[4]: https://www.drupal.org/project/library_manager
[5]: https://www.drupal.org/project/drupal/issues/2940481
[6]: https://drupal.stackexchange.com/questions/276779/dynamically-remove-library-based-on-user-role-cache-context-not-working
[7]: https://www.drupal.org/forum/general/general-discussion/2024-02-20/solved-how-to-convert-markdown-into-drupal-forumissue-compatible-format
[8]: https://www.drupal.org/project/infrastructure/issues/3422234
[9]: https://git.drupalcode.org/project/theme_test_not_sandboxed
[10]: https://www.drupal.org/docs/develop/managing-a-drupalorg-theme-module-or-distribution-project/maintainership/module-categories
