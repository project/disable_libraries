<?php declare(strict_types = 1);

namespace Drupal\Tests\disable_libraries\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\disable_libraries\LibraryDisabler;

/**
 * Not really used for actual testing, but nice to keep around as cheet sheat
 * with a working example for possible future kernel tests
 *
 * This test needs around 1.2 seconds to run. The unit test takes only 0.02 seconds.
 *
 * The kernel test requires a database!
 * Example setting for phpunit.xml in root folder:
 * `<env name="SIMPLETEST_DB" value="sqlite://../../db/kernel-test.sqlite"/>`
 * @see /path/to/drupal/web/core/phpunit.xml.dist
 */
class LibraryDisablerTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'disable_libraries',
  ];

  public function setUp(): void {
    parent::setup();
    $this->installConfig(['disable_libraries']);
  }
  /**
   * ReflectionClass for testing private method
   */
  protected function getContext(): string {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $property = $class->getProperty('context');
    return $property->getValue(null);
  }

  public function testGetContext() {
    LibraryDisabler::init();
    $this->assertEquals('anonymous', $this->getContext());
  }

  public function testHandleLibraries() {

    $config = $this->config('disable_libraries.settings');

    $extension = 'drinimal';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];

    $config->set('allowedAnon', ['drinimal']);
    $config->save();
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);
    $this->assertEquals($original, $libraries);


    LibraryDisabler::reset();
    $config->set('disableWebformCdn', false);
    $config->save();
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);
    $this->assertEquals($original, $libraries);


    LibraryDisabler::reset();
    $config->set('disableWebformCdn', true);
    $config->set('allowThemeAutomatically', false);
    $config->set('allowedAnon', []);
    $config->save();
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);
    $this->assertEquals([], $libraries);
  }

}
