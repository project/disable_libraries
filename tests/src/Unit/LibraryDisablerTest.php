<?php declare(strict_types = 1);

namespace Drupal\Tests\disable_libraries\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\disable_libraries\LibraryDisabler;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Theme\ActiveTheme;


/**
 * Disabling libraries was a surprisingly complex problem. So I finally
 * started to learn phpunit. If you see some wrong things or bad practices,
 * let me know.
 *
 * TODO: cleanup
 */
class LibraryDisablerTest extends UnitTestCase {

  protected $configFactory;

  // used in setUp method
  protected $baseTheme = [
    'name' => 'drinimal',
    'libraries' => [
      'drinimal/visually-hidden',
    ],
    'base_theme_extensions' => [],
    'libraries_override' => [
      'system/base' => false,
    ],
    'libraries_extend' => [],
  ];

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

    // set default to anonymous user
    $this->setIsAuthenticated(false);

    // set default to "drinimal" base theme as active theme
    $this->setActiveTheme($this->baseTheme);

    // set to empty config to test the default behavior when config is not
    // installed or deleted
    $this->setConfig([]);

    LibraryDisabler::reset();
  }

  protected function setConfig(array $config): void {
    $configFactory = $this->getConfigFactoryStub([
      'disable_libraries.settings' => $config,
    ]);
    \Drupal::getContainer()->set('config.factory', $configFactory);
  }

  protected function setIsAuthenticated(bool $status): void {
    $account = $this->getMockBuilder('Drupal\Core\Session\AccountProxy')
      ->onlyMethods(['isAuthenticated'])
      ->disableOriginalConstructor()
      ->getMock();
    $account->method('isAuthenticated')->willReturn($status);
    \Drupal::getContainer()->set('current_user', $account);
  }

  protected function setActiveTheme(array $themeConfig): void {
    $activeTheme = new ActiveTheme($themeConfig);

    $themeManager = $this->getMockBuilder('Drupal\Core\Theme\ThemeManager')
      ->onlyMethods(['getActiveTheme'])
      ->disableOriginalConstructor()
      ->getMock();
    $themeManager->method('getActiveTheme')->willReturn($activeTheme);
    \Drupal::getContainer()->set('theme.manager', $themeManager);
  }

  /**
   * for webform cdn disabler
   */
  protected function setLibraryFinder(string|false $value): void {
    $finder = $this->getMockBuilder('Drupal\Core\Asset\LibrariesDirectoryFileFinder')
      ->onlyMethods(['find'])
      ->disableOriginalConstructor()
      ->getMock();
    $finder->method('find')->willReturn($value);
    \Drupal::getContainer()->set('library.libraries_directory_file_finder', $finder);
  }

  /**
   * ReflectionClass for testing private method
   */
  protected function isAllowed(string $module, ?string $library = null,  ?string $index = null, ?string $type = null): ?bool {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $method = $class->getMethod('isAllowed');
    return $method->invokeArgs(null, [$module, $library, $index, $type]);
  }
  /**
   * ReflectionClass for testing private method
   */
  protected function isDenied(string $module, ?string $library = null,  ?string $index = null, ?string $type = null): ?bool {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $method = $class->getMethod('isDenied');
    return $method->invokeArgs(null, [$module, $library, $index, $type]);
  }
  /**
   * ReflectionClass for testing private method
   */
  protected function getMode(): string {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $property = $class->getProperty('mode');
    return $property->getValue(null);
  }
  /**
   * ReflectionClass for testing private method
   */
  protected function getContext(): string {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $property = $class->getProperty('context');
    return $property->getValue(null);
  }
  /**
   * ReflectionClass for testing private method
   */
  protected function getNestedList(): array {
    $class = new \ReflectionClass('\Drupal\disable_libraries\LibraryDisabler');
    $method = $class->getMethod('getNestedList');
    return $method->invoke(null);
  }

  public function testDefaultSettingsForBaseTheme() {

    $extension = 'drinimal';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];
    $librariesAfterAnonAllowed = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];
    $librariesAfterAuthDenied = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];

    LibraryDisabler::reset();
    LibraryDisabler::init();
    // test config
    $this->assertEquals('anonymous', $this->getContext());
    $this->assertEquals('allow', $this->getMode());
    $this->assertEquals([], $this->getNestedList());

    // test anonymous user
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnonAllowed, $libraries);

    // test authenticated user
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('authenticated', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([], $this->getNestedList());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAuthDenied, $libraries);
  }

  public function testDefaultSettingsForModule() {

    $extension = 'some_module';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];
    $librariesAfterAnon = [];
    $librariesAfterAuth = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];

    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('anonymous', $this->getContext());

    // test anonymous user
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnon, $libraries);

    // test authenticated user
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('authenticated', $this->getContext());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAuth, $libraries);
  }

  public function testDefaultSettingsForWebform() {

    $extension = 'webform';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
        'cdn' => [
          '/path/to/file' => 'https://cdn.example.com',
        ],
        'directory' => 'name_of_directory_inside_libraries_dir',
      ],
    ];
    $librariesAfterAnon = [];
    $librariesAfterAuth = [];

    // add assets finder mock
    $this->setLibraryFinder(false);

    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('anonymous', $this->getContext());

    // test anonymous user
    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnon, $libraries);

    // test authenticated user
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('authenticated', $this->getContext());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAuth, $libraries);
  }

  public function testCdnAllowedForWebform() {

    $extension = 'webform';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
        'cdn' => [
          '/path/to/file' => 'https://cdn.example.com',
        ],
        'directory' => 'name_of_directory_inside_libraries_dir',
      ],
    ];
    $librariesAfterAnon = [];
    $librariesAfterAuth = $original;

    $this->setConfig([
      'disableWebformCdn' => false,
    ]);

    // add assets finder mock
    $this->setLibraryFinder(false);

    LibraryDisabler::reset();
    LibraryDisabler::init();

    // make sure, that default settings from `setUp` method are correct
    $this->assertEquals('anonymous', $this->getContext());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnon, $libraries);

    // test authenticated user
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('authenticated', $this->getContext());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAuth, $libraries);
  }

  /**
   * TODO: more tests
   */
  public function testExternalLibraries() {

    $extension = 'some_module';

    $original = [
      'visually-hidden' => [
        'css' => [
          'layout' => [
            // type: external is set, e. g. after webform transformed it's libraries
            'css/visually-hidden.css' => [
              'type' => 'external',
            ],
          ],
          'component' => [
            '//cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.css' => [],
            'http://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.css' => [],
            'https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.css' => [],
          ],
        ],
        'js:' => [
          // asset key is an external url without type, e. g. in photoswipe <5.0
          '//cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js' => [
            'minified' => true,
          ],
          'http//cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js' => [
            'minified' => true,
          ],
          'https//cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js' => [
            'minified' => true,
          ],
        ],
      ],
    ];
    $librariesAfterAnon = [];
    $librariesAfterAuth = [];

    $this->setConfig([
      'disableExternalLibraries' => true,
    ]);

    LibraryDisabler::reset();
    LibraryDisabler::init();

    // make sure, that default settings from `setUp` method are correct
    $this->assertEquals('anonymous', $this->getContext());


    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnon, $libraries);

    // test authenticated user
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertEquals('authenticated', $this->getContext());

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAuth, $libraries);
  }

  public function testIsAllowed() {

    $this->setConfig([
      'anonMode' => 'allow',
      'allowedAnon' => [],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertFalse($this->isAllowed('test'));


    $this->setConfig([
      'anonMode' => 'allow',
      'allowedAnon' => [
        'test',
      ],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // module allowed
    $this->assertTrue($this->isAllowed('test'));
    // module not allowed
    $this->assertFalse($this->isAllowed('some_module'));
    $this->assertFalse($this->isAllowed('some_module/global-styling'));


    $this->setConfig([
      'anonMode' => 'allow',
      'allowedAnon' => [
        'test',
        'some_module/global-styling',
        'some_module/some_js/js',
        'other/lib/css/base',
        'other/lib/js/invalid',
      ],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // module allowed
    $this->assertTrue($this->isAllowed('test'));
    $this->assertTrue($this->isAllowed('test/some/thing'));

    // unknown state
    $this->assertNull($this->isAllowed('some_module'));

    // some libraries allowed
    $this->assertTrue($this->isAllowed('some_module/global-styling'));
    $this->assertTrue($this->isAllowed('some_module/global-styling/js'));
    $this->assertTrue($this->isAllowed('some_module/global-styling/css'));
    $this->assertFalse($this->isAllowed('some_module/some_js/css'));

    // some parts of libraries are allowed
    $this->assertTrue($this->isAllowed('other/lib/css/base'));
    $this->assertFalse($this->isAllowed('other/lib/css/component'));
    $this->assertFalse($this->isAllowed('other/lib/js'));

    $this->assertTrue($this->isAllowed('other/lib/css/base/invalid'));
    $this->assertFalse($this->isAllowed('other/lib/js/invalid'));

    // method input variations
    $this->assertTrue($this->isAllowed('other/lib/css/base'));
    $this->assertTrue($this->isAllowed('other', 'lib/css/base'));
    $this->assertTrue($this->isAllowed('other', 'lib', 'css', 'base'));
  }

  public function testIsDenied() {

    $this->setConfig([
      'anonMode' => 'deny',
      'deniedAnon' => [],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();
    $this->assertFalse($this->isDenied('test'));
    $this->assertTrue($this->isAllowed('test'));


    $this->setConfig([
      'anonMode' => 'deny',
      'deniedAnon' => [
        'test',
      ],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('anonymous', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([
      'test' => [],
    ], $this->getNestedList());

    // module denied
    $this->assertTrue($this->isDenied('test'));
    $this->assertFalse($this->isAllowed('test'));
    // module not denied
    $this->assertFalse($this->isDenied('some_module'));
    $this->assertTrue($this->isAllowed('some_module'));
    $this->assertFalse($this->isDenied('some_module/global-styling'));


    $this->setConfig([
      'anonMode' => 'deny',
      'deniedAnon' => [
        'test',
        'some_module/global-styling',
        'some_module/some_js/js',
        'other/lib/css/base',
        'other/lib/js/invalid',
      ],
    ]);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('anonymous', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([
      'test' => [],
      'some_module' => [
        'global-styling' => [],
        'some_js' => [
          'js' => [],
        ],
      ],
      'other' => [
        'lib' => [
          'css' => [
            'base' => [],
          ],
        ],
      ],
    ], $this->getNestedList());

    // module denied
    $this->assertTrue($this->isDenied('test'));
    $this->assertTrue($this->isDenied('test/some/thing'));
    $this->assertTrue($this->isDenied('test', 'some/thing'));

    // unknown state
    $this->assertNull($this->isDenied('some_module'));

    // some libraries denied
    $this->assertTrue($this->isDenied('some_module/global-styling'));
    $this->assertTrue($this->isDenied('some_module/global-styling/js'));
    $this->assertTrue($this->isDenied('some_module/global-styling/css'));
    $this->assertFalse($this->isDenied('some_module/some_js/css'));

    // some parts of libraries are denied
    $this->assertTrue($this->isDenied('other/lib/css/base'));
    $this->assertFalse($this->isDenied('other/lib/css/component'));
    $this->assertFalse($this->isDenied('other/lib/js'));

    $this->assertTrue($this->isDenied('other/lib/css/base/invalid'));

    $this->assertFalse($this->isDenied('other/lib/js/invalid'));

    // method input variations
    $this->assertTrue($this->isDenied('other/lib/css/base'));
    $this->assertTrue($this->isDenied('other', 'lib/css/base'));
    $this->assertTrue($this->isDenied('other', 'lib', 'css', 'base'));

  }

  public function testhandleLibrariesAnonymousDeniedNoTheme() {

    $this->setIsAuthenticated(false);

    $this->setConfig([
      'anonMode' => 'deny',
      'deniedAnon' => [
        'test',
        'some_module/global-styling',
        'some_module/some_js/js',
        'other/lib/css/base',
        'other/lib/js/invalid',
      ],
      'allowThemeAutomatically' => false,
    ]);

    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('anonymous', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([
      'test' => [],
      'some_module' => [
        'global-styling' => [],
        'some_js' => [
          'js' => [],
        ],
      ],
      'other' => [
        'lib' => [
          'css' => [
            'base' => [],
          ],
        ],
      ],
    ], $this->getNestedList());

    $extension = 'some_module';

    $original = [
      'global-styling' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
        'cdn' => [
          '/path/to/file' => 'https://cdn.example.com',
        ],
      ],
      'some_js' => [
        'js' => [
          'path/to/script.js' => []
        ],
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
      'admin' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];
    $librariesAfterAnon = [
      'some_js' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
      'admin' => [
        'css' => [
          'layout' => [
            'css/visually-hidden.css' => [],
          ],
        ],
      ],
    ];

    $libraries = $original;
    LibraryDisabler::handleLibraries($libraries, $extension);

    $this->assertEquals($librariesAfterAnon, $libraries);

  }

  public function testModifyActiveTheme() {

    $themeExtension = $this->getMockBuilder('Drupal\Core\Extension\Extension')
      ->onlyMethods(['getName'])
      ->disableOriginalConstructor()
      ->getMock();
    $themeExtension->method('getName')->willReturn('claro');
    $themeExtension->info = [
      'libraries-override' => [
        'system/base' => false,
      ],
      'libraries' => [
        'drinimal/visually-hidden',
      ],
      'libraries-extend' => [
        'core/drupal.ajax' => [
          'claro/ajax',
        ],
        'system/admin' => [
          'claro/system.admin',
        ],
      ],
    ];

    $baseThemeExtension = $this->getMockBuilder('Drupal\Core\Extension\Extension')
      ->onlyMethods(['getName'])
      ->disableOriginalConstructor()
      ->getMock();
    $baseThemeExtension->method('getName')->willReturn('drinimal');
    $baseThemeExtension->info = [
      'libraries-override' => [
        'system/base' => false,
      ],
      'libraries' => [
        'drinimal/visually-hidden',
      ],
    ];

    $this->setConfig([
      'allowThemeAutomatically' => false,
      'authMode' => 'allow',
      'allowedAuth' => [
        'system',
      ],
    ]);

    $this->setIsAuthenticated(true);

    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('authenticated', $this->getContext());
    $this->assertEquals('allow', $this->getMode());
    $this->assertEquals([
      'system' => [],
    ], $this->getNestedList());

    $theme = clone $themeExtension;
    $baseThemes = [clone $baseThemeExtension];

    LibraryDisabler::modifyActiveThemeDuringInit($theme, $baseThemes);

    $this->assertEquals([], $theme->info['libraries-extend']);




    $this->setConfig([
      'allowThemeAutomatically' => true,
      'authMode' => 'allow',
      'allowedAuth' => [
        'system',
      ],
    ]);
    LibraryDisabler::reset();

    $theme = clone $themeExtension;
    $baseThemes = [clone $baseThemeExtension];

    LibraryDisabler::modifyActiveThemeDuringInit($theme, $baseThemes);

    $this->assertEquals([
      'system/admin' => [
        'claro/system.admin',
      ],
    ], $theme->info['libraries-extend']);




    $this->setConfig([
      'allowThemeAutomatically' => false,
      'authMode' => 'deny',
      'deniedAuth' => [
        'claro',
      ],
    ]);
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('authenticated', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([
      'claro' => [],
    ], $this->getNestedList());

    $theme = clone $themeExtension;
    $baseThemes = [clone $baseThemeExtension];

    LibraryDisabler::modifyActiveThemeDuringInit($theme, $baseThemes);

    $this->assertEquals([], $theme->info['libraries-extend']);




    $this->setConfig([
      'allowThemeAutomatically' => false,
      'authMode' => 'deny',
      'deniedAuth' => [
        'claro/system.admin',
      ],
    ]);
    $this->setIsAuthenticated(true);
    LibraryDisabler::reset();
    LibraryDisabler::init();

    // test config
    $this->assertEquals('authenticated', $this->getContext());
    $this->assertEquals('deny', $this->getMode());
    $this->assertEquals([
      'claro' => [
        'system.admin' => [],
      ],
    ], $this->getNestedList());

    $theme = clone $themeExtension;
    $baseThemes = [clone $baseThemeExtension];

    LibraryDisabler::modifyActiveThemeDuringInit($theme, $baseThemes);

    $this->assertEquals([
      'core/drupal.ajax' => [
        'claro/ajax',
      ],
    ], $theme->info['libraries-extend']);

  }

  // Set invalid config and run tests for default config again
  public function testInvalidConfig() {

    $config = [
      'allowThemeAutomatically' => 'string',
      'authMode' => 'invalid',
      'anonMode' => 'invalid',
      'allowedAuth' => false,
      'deniedAuth' => null,
      'allowedAnon' => false,
      'deniedAnon' => null,
      'disableWebformCdn' => null,
    ];

    $this->setConfig($config);
    $this->testDefaultSettingsForBaseTheme();

    $this->setUp();
    $this->setConfig($config);
    $this->testDefaultSettingsForModule();

    $this->setUp();
    $this->setConfig($config);
    $this->testCdnAllowedForWebform();

    $config = [
      'disableWebformCdn' => '',
    ];
    $this->setUp();
    $this->setConfig($config);
    $this->testDefaultSettingsForBaseTheme();
  }

}
