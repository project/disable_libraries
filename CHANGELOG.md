# Changelog

## Upcoming

* fixed unit/kernel test incompatibilities with Drupal 11

## 0.1.2

* added "Configure" link on "Extend" page - Thanks @ressa
* added check, if asset is external url (type is unreliable)

## 0.1.1

* make sure to alter libraries after all other modules
* fixed possible php warnings when disabling webform cdn - see: https://www.drupal.org/project/disable_libraries/issues/3443762
* renamed `disableCDN` config key to `disableWebformCdn`
* added experimental `disableExternalLibraries` option to disable __all__ external libraries
* changed some labels and descriptions in settings form
* minor code cleanup

## 0.1.0

* initial release
