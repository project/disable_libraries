<?php declare(strict_types = 1);

namespace Drupal\disable_libraries;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Config\Config;

/**
 * Apply allow/deny list to LibraryDiscoveryCollector via
 * HOOK_library_info_alter and to ThemeInitialization via decorator
 *
 * This class can run in four states:
 * - authenticated, allow
 * - authenticated, deny
 * - anonymous, allow
 * - anonymous, deny
 */
class LibraryDisabler {

  protected static bool $isInitialized = false;
  protected static bool $areThemesInitialized = false;

  // Config object of 'disable_libraries.settings'
  protected static ?Config $config = null;

  // current context (authenticated|anonymous)
  protected static ?string $context = null;

  // current mode (allow|deny)
  protected static ?string $mode = null;

  // allow/deny list in current context and mode
  protected static ?array $list = null;

  // disable cdn usage of webform module
  protected static bool $disableWebformCdn = true;

  // experimental
  // TODO: default to true when stable
  protected static bool $disableExternalLibraries = false;

  /**
   * Initialize properties and config
   */
  public static function init(bool $withTheme = false): void {

    if (!self::$isInitialized) {
      self::$config = \Drupal::config('disable_libraries.settings');

      $isLoggedIn = \Drupal::currentUser()->isAuthenticated();
      self::$context = $isLoggedIn ? 'authenticated' : 'anonymous';

      $modeKey = self::$context === 'authenticated' ? 'authMode' : 'anonMode';
      $mode = self::$config->get($modeKey);
      // set default if config is missing or invalid
      if (!$mode || !is_string($mode) || !in_array($mode, ['deny', 'allow'])) {
        $mode = self::$context === 'authenticated' ? 'deny' : 'allow';
      }
      self::$mode = $mode;

      // init list
      self::getNestedList();

      // disable cdn usage of webform module (default: true)
      $disableWebformCdn = self::$config->get('disableWebformCdn');
      if (!is_bool($disableWebformCdn)) {
        $disableWebformCdn = true;
      }
      self::$disableWebformCdn = $disableWebformCdn;

      // disable all external libraries (experimental, default: false)
      // TODO: default to true when stable
      $disableExternalLibraries = self::$config->get('disableExternalLibraries');
      if (!is_bool($disableExternalLibraries)) {
        $disableExternalLibraries = false;
      }
      self::$disableExternalLibraries = $disableExternalLibraries;

      self::$isInitialized = true;
    }

    // This part should never be called in a production system, because
    // `self::$areThemesInitialized` is set during theme initalization.
    // It's still useful to keep around for unit testing and in case some
    // logic during theme initialization changes in the future.
    if ($withTheme && !self::$areThemesInitialized && (self::$mode === 'allow')) {

      // Allow libraries of current theme and parent/base themes by default
      // Disabling this is a bad idea, except for Naked CSS/JS Days
      $allowThemeAutomatically = self::$config->get('allowThemeAutomatically') ?? true;

      if ($allowThemeAutomatically) {
        $activeTheme = \Drupal::service('theme.manager')->getActiveTheme();

        // If a theme lib is explicitely allowed, assume, that others are denied. If not, assume, that all theme libs are allowed.
        $name = $activeTheme->getName();
        self::$list[$name] = self::$list[$name] ?? [];

        $baseThemes = $activeTheme->getBaseThemeExtensions();
        foreach ($baseThemes as $theme) {
          $name = $activeTheme->getName();
          self::$list[$name] = self::$list[$name] ?? [];
        }
      }
      self::$areThemesInitialized = true;
    }
  }

  /**
   * Reset all static variables for subsequent runs while unit testing
   */
  public static function reset(): void {
    self::$list = null;

    self::$disableWebformCdn        = true;
    self::$disableExternalLibraries = false;

    self::$isInitialized        = false;
    self::$areThemesInitialized = false;

    // self::$config      = null;
    // self::$context     = null;
    // self::$mode        = null;
  }

  /**
   * Transform flat list from user config (e. g. 'claro/system.admin/css')
   * into nested array, that can be compared with libraries.
   *
   * Example:
   * input: 'claro/system.admin/css'
   * output: ['claro' => ['system.admin' => ['css' => []]]]
   */
  protected static function getNestedList(): array {

    if (!self::$list) {

      // get flat list
      $context = self::$context;
      $listKey = self::$mode === 'allow' ? 'allowed' : 'denied';
      $listKey .= $context === 'authenticated' ? 'Auth' : 'Anon';
      $list    = self::$config->get($listKey);
      $list    = is_array($list) ? $list : [];

      // build nested array list
      // TODO: sanitize values: trim whitespace, handle empty/invalid values
      $allowList = [];
      foreach ($list as $v) {
        if (!\str_contains($v, '/')) {
          $allowList[$v] = $allowList[$v] ?? [];
          continue;
        }

        [$module, $library, $index, $type] = \array_pad(\explode('/', $v, 4), 4, null);

        // prevent allowing parent keys for invalid keys
        if ($type && $index !== 'css') continue;

        $allowList[$module] = $allowList[$module] ?? [];
        if ($library) {
          $allowList[$module][$library] = $allowList[$module][$library] ?? [];
        }
        if ($index) {
          $allowList[$module][$library][$index] = $allowList[$module][$library][$index] ?? [];
        }
        if ($type) {
          $allowList[$module][$library][$index][$type] = [];
        }
      }

      self::$list = $allowList;
    }

    return self::$list;
  }

  /**
   * Check, if module/library is allowed.
   *
   * Usage:
   * - isAllowed('module')
   * - isAllowed('module', 'library')
   * - isAllowed('module', 'library', 'index', 'type')
   * or:
   * - isAllowed('module/library')
   * - isAllowed('module/library/index')
   * - isAllowed('module/library/index/type')
   * or:
   * - isAllowed('module', 'library/index')
   * - isAllowed('module', 'library/index/type')
   *
   * See `isListed()` method for more details.
   */
  protected static function isAllowed(string $module, ?string $library = null,  ?string $index = null, ?string $type = null): ?bool {
    $isListed = self::isListed($module, $library, $index, $type);
    if ('allow' === self::$mode) return $isListed;
    return null === $isListed ? null : !$isListed;
  }

  /**
   * Check, if module/library is denied.
   *
   * Usage:
   * - isDenied('module')
   * - isDenied('module', 'library')
   * - isDenied('module', 'library', 'index', 'type')
   * or:
   * - isDenied('module/library')
   * - isDenied('module/library/index')
   * - isDenied('module/library/index/type')
   * or:
   * - isDenied('module', 'library/index')
   * - isDenied('module', 'library/index/type')
   *
   * See `isListed()` method for more details.
   */
  protected static function isDenied(string $module, ?string $library = null,  ?string $index = null, ?string $type = null): ?bool {
    $isListed = self::isListed($module, $library, $index, $type);
    if ('deny' === self::$mode) return $isListed;
    return null === $isListed ? null : !$isListed;
  }

  /**
   * Check, if module/library is listed.
   *
   * Returns null for unknown state, when the list is more specific then the the check.
   *
   * Examples:
   * allow/deny list: 'claro/system.admin/css'
   * - isListed('claro/system.admin/css') --> true
   * - isListed('claro/system.admin/js') --> false
   * - isListed('claro/system.admin') --> null
   * allow/deny list: 'claro/system.admin'
   * - isListed('claro/system.admin/css') --> true
   * - isListed('claro/system.admin/js') --> true
   * - isListed('claro/system.admin') --> true
   *
   * @return true|false|null
   */
  protected static function isListed(string $module, ?string $library = null,  ?string $index = null, ?string $type = null): ?bool {

    // allow short notation
    // e. g. `('module/library/css/base')`
    if (\str_contains($module, '/')) {
      [$module, $library, $index, $type] = \array_pad(\explode('/', $module, 4), 4, null);
    }
    // allow short notation
    // e. g. `('module', 'library/css/base')`
    elseif ($library && \str_contains($library, '/')) {
      [$library, $index, $type] = \array_pad(\explode('/', $library, 3), 3, null);
    }

    // take care of invalid keys (too much slashes...)
    // e. g. module/lib/css/base/invalid would result in 'base/invalid' as type. If module/lib/css/base is denied, 'base/invalid' must be denied, too.
    if ($type && \str_contains($type, '/')) {
      [$type, $invalid] = \array_pad(\explode('/', $type, 2), 2, null);
    }

    // e. g. : claro
    if (!isset(self::$list[$module])) return false;
    if (empty(self::$list[$module])) return true;

    if (!$library) return null;

    // e. g. : claro/system.admin
    if (!isset(self::$list[$module][$library])) return false;
    if (empty(self::$list[$module][$library])) return true;

    if (!$index) return null;

    // e. g. : claro/system.admin/css
    if (!isset(self::$list[$module][$library][$index])) return false;
    if (empty(self::$list[$module][$library][$index])) return true;

    if ($type) {
      if ($index !== 'css') return false;

      // e. g. : claro/system.admin/css/base
      if (!isset(self::$list[$module][$library][$index][$type])) return false;

      return true;
    }

    return false;
  }

  /**
   * Helper to prevent IncompleteLibraryDefinitionException
   */
  protected static function isLibraryIncomplete(array $library): bool {
    return !isset($library['js'])
        && !isset($library['css'])
        && !isset($library['drupalSettings'])
        && !isset($library['dependencies']);
  }

  /**
   * Apply allow/deny list to library collector
   *
   * Blocks all libraries of disallowed modules
   * Meant to be used with HOOK_library_info_alter
   */
  public static function handleLibraries(array &$libraries, string $extension): void {

    if (!self::$areThemesInitialized) {
      self::init(true);
    }

    if ('allow' === self::$mode) {
      self::allowLibraries($libraries, $extension);
    }
    elseif ('deny' === self::$mode) {
      self::denyLibraries($libraries, $extension);
    }

    self::handleExternalLibraries($libraries, $extension);

  }

  /**
   * Unset denied libraries during HOOK_library_info_alter
   */
  protected static function denyLibraries(array &$libraries, string $extension): void {

    $status = self::isDenied($extension);
    if (true === $status) {
      $libraries = [];
      return;
    }
    if (false === $status) return;

    foreach ($libraries as $libraryName => $library) {

      // deny one library of a module
      // e. g. "views/views.module"
      $status = self::isDenied($extension, $libraryName);
      if (true === $status) {
        unset($libraries[$libraryName]);
        continue;
      }
      if (false === $status) continue;

      // deny part of one library of a module
      // e. g. "views/views.module/css"
      foreach(['css', 'js', 'drupalSettings'] as $key) {
        if (isset($libraries[$libraryName][$key],
          self::$list[$extension][$libraryName][$key])
        ) {
          unset($libraries[$libraryName][$key]);
        }
      }

      // prevent invalid library exception
      if (self::isLibraryIncomplete($libraries[$libraryName])) {
        unset($libraries[$libraryName]);
      }
    }

  }

  /**
   * Unset not-allowed libraries during HOOK_library_info_alter
   */
  protected static function allowLibraries(array &$libraries, string $extension): void {

    // deny complete module
    // e. g. "views"
    $status = self::isAllowed($extension);
    if (false === $status) {
      $libraries = [];
      return;
    }
    if (true === $status) return;

    foreach ($libraries as $libraryName => $library) {

      // deny one library of a module
      // e. g. "views/views.module"
      $status = self::isAllowed($extension, $libraryName);
      if (false === $status) {
        unset($libraries[$libraryName]);
        continue;
      }
      if (true === $status) continue;

      // deny part of one library of a module
      // e. g. "views/views.module/css"
      foreach(['css', 'js', 'drupalSettings'] as $key) {
        if (!isset($libraries[$libraryName][$key],
          self::$list[$extension][$libraryName][$key])
        ) {
          unset($libraries[$libraryName][$key]);
        }
      }

      // prevent invalid library exception
      if (self::isLibraryIncomplete($libraries[$libraryName])) {
        unset($libraries[$libraryName]);
      }
    }
  }

  /**
   * Unset external libraries during HOOK_library_info_alter
   *
   * @see https://www.drupal.org/docs/develop/creating-modules/adding-assets-css-js-to-a-drupal-module-via-librariesyml#external
   */
  protected static function handleExternalLibraries(array &$libraries, string $extension): void {

    // The webform module is a special case. It uses a non-standard 'cdn' key
    // in it's `*.libraries.yml` and replaces it's library definitions during
    // `HOOK_library_info_alter` if they are not present in the file system.
    if (('webform' === $extension) && self::$disableWebformCdn) {

      // no need to run custom webform logic, when all external libraries are
      // disabled
      if (!self::$disableExternalLibraries) {

        // This method runs after webform transformed it's libraries. To
        // prevent disabling cdn based libs, that are present in the file
        // system, the file finder service needs to run again (minimal
        // performance decrease due to `file_exists()` calls).
        // @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Asset!LibrariesDirectoryFileFinder.php/class/LibrariesDirectoryFileFinder/10
        $finder = \Drupal::service('library.libraries_directory_file_finder');

        foreach ($libraries as $libraryName => $library) {
          // @see web/modules/contrib/webform/includes/webform.libraries.inc
          if (!isset($library['cdn'], $library['directory'])) {
            continue;
          }
          if ($finder->find($library['directory'])) {
            continue;
          }
          unset($libraries[$libraryName]);
        }

      }
    }

    if (!self::$disableExternalLibraries) return;

    // unset external libraries
    foreach ($libraries as $libraryName => $library) {

      foreach ($library as $type => $value) {

        if (!in_array($type, ['css', 'js'])) continue;

        if ('js' === $type) {
          $assets = $value;
          foreach ($assets as $k => $asset) {
            // type: external is set, e. g. after webform transformed it's libraries
            $hasTypeExternal = isset($asset['type']) && 'external' === $asset['type'];
            // asset key is an external url without type, e. g. in photoswipe <5.0
            $isExternal = $hasTypeExternal ?: preg_match('#^http(s?)://|^//#', $k);
            if ($isExternal) {
              unset($libraries[$libraryName][$type][$k]);
            }
          }
          if (empty($libraries[$libraryName][$type])) {
            unset($libraries[$libraryName][$type]);
          }
        }
        if ('css' === $type) {
          foreach ($value as $group => $assets) {
            foreach ($assets as $k => $asset) {
              $hasTypeExternal = isset($asset['type']) && 'external' === $asset['type'];
              $isExternal = $hasTypeExternal ?: preg_match('#^http(s?)://|^//#', $k);
              if ($isExternal) {
                unset($libraries[$libraryName][$type][$group][$k]);
              }
            }
            if (empty($libraries[$libraryName][$type][$group])) {
              unset($libraries[$libraryName][$type][$group]);
            }
          }
          if (empty($libraries[$libraryName][$type])) {
            unset($libraries[$libraryName][$type]);
          }
        }
      }

      // prevent invalid library exception
      if (self::isLibraryIncomplete($libraries[$libraryName])) {
        unset($libraries[$libraryName]);
      }

    }

    // remove dependencies, that were disabled in the previous step
    foreach ($libraries as $libraryName => $library) {

      if (!isset($library['dependencies']) || empty($library['dependencies'])) {
        continue;
      }
      foreach ($library['dependencies'] as $k => $dependency) {
        [$module, $depLibraryName] = \explode('/', $dependency, 2);
        if ($module === $extension) {
          if (!isset($libraries[$depLibraryName])) {
            unset($libraries[$libraryName]['dependencies'][$k]);
          }
        }
      }
      if (empty($libraries[$libraryName]['dependencies'])) {
        unset($libraries[$libraryName]['dependencies']);
      }
      // prevent invalid library exception
      if (self::isLibraryIncomplete($libraries[$libraryName])) {
        unset($libraries[$libraryName]);
      }

    }

  }

  /**
   * Remove library extensions ('libraries-extend' keys) in active theme if they are denied.
   *
   * Called from `ThemeInitializationOverride::getActiveTheme()`, which
   * extends the core `ThemeInitialization` class by decorating the
   * `theme.initialization` service.
   *
   * @param $theme      Extension object of active theme
   * @param $baseThemes array of Extension objects of base themes
   */
  public static function modifyActiveThemeDuringInit(Extension &$theme, array &$baseThemes): void {

    if (!self::$isInitialized) {
      self::init();
    }

    $themes = array_merge([$theme], $baseThemes);

    $allowThemeAutomatically = self::$config->get('allowThemeAutomatically') ?? true;
    // add active theme and base themes to allow list
    if ($allowThemeAutomatically && (self::$mode === 'allow')) {
      // init list
      self::getNestedList();
      foreach ($themes as $k => $thm) {
        self::$list[$thm->getName()] = [];
      }
    }
    // no need to call the theme manager anymore, because the list of themes
    // is already applied during theme initialization
    self::$areThemesInitialized = true;

    foreach ($themes as $k => $thm) {

      if (empty($thm->info['libraries-extend'])) {
        continue;
      }

      $name = $thm->getName();

      if (self::$mode === 'allow') {

        $status = self::isAllowed($name);
        if (false === $status) {
          // theme is not allowed, therefore all theme extensions aren't allowed
          $thm->info['libraries-extend'] = [];;
          continue;
        }

        foreach ($thm->info['libraries-extend'] as $original => $replacements) {

          $status = self::isAllowed($original);
          if (true !== $status) {
            unset($thm->info['libraries-extend'][$original]);
            continue;
          }

          foreach ($replacements as $k => $replacement) {

            $status = self::isAllowed($replacement);
            if (true !== $status) {
              unset($thm->info['libraries-extend'][$original][$k]);
            }
          }
          if (empty($thm->info['libraries-extend'][$original])) {
            unset($thm->info['libraries-extend'][$original]);
          }
        }

      }

      elseif (self::$mode === 'deny') {

        $status = self::isDenied($name);
        if (true === $status) {
          // theme is not allowed, therefore all theme extensions aren't allowed
          $thm->info['libraries-extend'] = [];
          continue;
        }

        foreach ($thm->info['libraries-extend'] as $original => $replacements) {

          $status = self::isDenied($original);
          if (true === $status) {
            unset($thm->info['libraries-extend'][$original]);
            continue;
          }

          foreach ($replacements as $k => $replacement) {

            $status = self::isDenied($replacement);
            if (false !== $status) {
              unset($thm->info['libraries-extend'][$original][$k]);
            }
          }
          if (empty($thm->info['libraries-extend'][$original])) {
            unset($thm->info['libraries-extend'][$original]);
          }
        }

      }

    }

  }

}
