<?php

namespace Drupal\disable_libraries\Theme;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\disable_libraries\LibraryDisabler;

/**
 * Provides the theme initialization logic.
 */
class ThemeInitializationOverride extends ThemeInitialization {

  /**
   * Check, if active theme is denied, but tries to extend an allowed module.
   * If so, clean the 'libraries-extend' key to prevent a 500 error in
   * \Drupal\Core\Asset\LibraryDiscoveryCollector::applyLibrariesExtend()
   */
  public function getActiveTheme(Extension $theme, array $base_themes = []) {

    LibraryDisabler::modifyActiveThemeDuringInit($theme, $base_themes);

    return parent::getActiveTheme($theme, $base_themes);

  }
}
