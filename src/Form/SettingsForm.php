<?php declare(strict_types = 1);

namespace Drupal\disable_libraries\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure disable_libraries settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * List mode select box options
   */
  public $modeOptions = [
    'allow' => 'allow',
    'deny'  => 'deny',
  ];

  /**
   * Convert newline separated string to array
   */
  public function splitByNewLine(string $text): array {
    return preg_split('~\R+~u', $text, -1, PREG_SPLIT_NO_EMPTY);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'disable_libraries_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['disable_libraries.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    // form api: https://www.drupal.org/docs/drupal-apis/form-api/form-render-elements
    // form types: https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element/8.2.x

    $config = $this->config('disable_libraries.settings');

    $form['anonymous'] = [
      '#type' => 'details',
      '#title' => t('Config for anonymous users'),
      '#open' => TRUE,
    ];
    $form['anonymous']['anonMode'] = [
      '#type' => 'select',
      '#options' => $this->modeOptions,
      '#title' => $this->t('List mode for anonymous users'),
      '#description' => $this->t('With an allow list all libraries, except allowed ones, are disabled. With a deny list, all libraries, except denied ones, are enabled.<br />Default: allow'),
      '#default_value' => $config->get('anonMode'),
    ];
    $value = $config->get('deniedAnon') ?? [];
    $value = implode("\r\n", $value);
    $form['anonymous']['deniedAnon'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Deny list for anonymous users'),
      '#description' => $this->t('List of denied modules. Write each module on a separate line.'),
      '#default_value' => $value,
    ];
    $value = $config->get('allowedAnon') ?? [];
    $value = implode("\r\n", $value);
    $form['anonymous']['allowedAnon'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allow list for anonymous users'),
      '#description' => $this->t('List of allowed modules. Write each module on a separate line.'),
      '#default_value' => $value,
    ];

    $form['authenticated'] = [
      '#type' => 'details',
      '#title' => t('Config for authenticated users'),
      '#open' => TRUE,
    ];
    $form['authenticated']['authMode'] = [
      '#type' => 'select',
      '#options' => $this->modeOptions,
      '#title' => $this->t('List mode for authenticated users'),
      '#description' => $this->t('With an allow list all libraries, except allowed ones, are disabled. With a deny list all libraries, except denied ones, are enabled.<br />Default: deny'),
      '#default_value' => $config->get('authMode'),
    ];
    $value = $config->get('deniedAuth') ?? [];
    $value = implode("\r\n", $value);
    $form['authenticated']['deniedAuth'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Deny list for authenticated users'),
      '#description' => $this->t('List of denied modules. Write each module on a separate line.'),
      '#default_value' => $value,
    ];
    $value = $config->get('allowedAuth') ?? [];
    $value = implode("\r\n", $value);
    $form['authenticated']['allowedAuth'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allow list for authenticated users'),
      '#description' => $this->t('List of allowed modules. Write each module on a separate line.'),
      '#default_value' => $value,
    ];

    $form['other'] = [
      '#type' => 'details',
      '#title' => t('Global config'),
      '#open' => TRUE,
    ];
    $form['other']['allowThemeAutomatically'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically allow libraries from active theme and base themes.'),
      '#description' => $this->t("Only effective in allow mode. Make sure to allow your theme and all it's base themes when disabling this setting.<br />Default: enabled."),
      '#default_value' => $config->get('allowThemeAutomatically'),
    ];
    $url = 'https://www.drupal.org/project/webform/issues/3343915';
    $form['other']['disableWebformCdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable automatic CDN usage of webform module'),
      '#description' => $this->t('CDNs are enabled by default (see <a href=":url">related issue</a>). To prevent GDPR or security disasters, they should be disabled.<br />Default: enabled.', [':url' => $url]),
      '#default_value' => $config->get('disableWebformCdn'),
    ];

    $form['experimental'] = [
      '#type' => 'details',
      '#title' => t('Experimental config'),
      '#description' => $this->t('Experimental config is not considered stable (yet) and disabled by default'),
      '#open' => TRUE,
    ];
    $form['experimental']['disableExternalLibraries'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable external libraries of all modules.'),
      '#description' => $this->t('Some modules (e. g. webform) have CDNs enabled by default. To prevent GDPR or security disasters, they should be disabled.'),
      '#default_value' => $config->get('disableExternalLibraries'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // TODO: Validate the form - low priority, because the module sanitizes
    // invalid config to default values
    // Example:
    // @code
    //   if ($form_state->getValue('example') === 'wrong') {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('The value is not correct.'),
    //     );
    //   }
    // @endcode
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $config = $this->config('disable_libraries.settings');

    $hasChanged = false;

    $configNames = [
      'authMode',
      'anonMode',
    ];
    foreach ($configNames as $name) {
      $value = $form_state->getValue($name);
      if ($config->get($name) !== $value) $hasChanged = true;
      $config->set($name, $value);
    }

    $configNames = [
      'allowThemeAutomatically',
      'disableWebformCdn',
      'disableExternalLibraries',
    ];
    foreach ($configNames as $name) {
      $value = (bool) $form_state->getValue($name);
      if ($config->get($name) !== $value) $hasChanged = true;
      $config->set($name, $value);
    }

    $configNames = [
      'allowedAuth',
      'allowedAnon',
      'deniedAuth',
      'deniedAnon',
    ];
    foreach ($configNames as $name) {
      $value = $this->splitByNewLine($form_state->getValue($name));
      if ($config->get($name) !== $value) $hasChanged = true;
      $config->set($name, $value);
    }

    $config->save();

    // flush cache automatically
    if ($hasChanged) \drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
