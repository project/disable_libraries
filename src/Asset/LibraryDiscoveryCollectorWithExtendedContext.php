<?php

namespace Drupal\disable_libraries\Asset;

use Drupal\Core\Asset\LibraryDiscoveryCollector;

/**
 * Decorates the LibraryDiscoveryCollector service to
 * add user role (authenticated/anonymous) to cache ID
 *
 * Based on Orlando Thöny's work:
 * @see https://drupal.stackexchange.com/questions/276779/dynamically-remove-library-based-on-user-role-cache-context-not-working
 */
class LibraryDiscoveryCollectorWithExtendedContext extends LibraryDiscoveryCollector {

  /**
   * @return string cache ID
   */
  protected function getCid() {
    if (!isset($this->cid)) {

      // set cache id to default --> 'library_info:theme_name:anonymous'
      parent::getCid();

      $isLoggedIn = \Drupal::currentUser()->isAuthenticated();

      // append user role to cache id --> 'library_info:theme_name:anonymous'
      $this->cid .= ':' . ($isLoggedIn ? 'authenticated' : 'anonymous');

    }

    return $this->cid;
  }

}
